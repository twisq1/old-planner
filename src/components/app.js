import React, { Component } from "react";
import { Spinner } from "@blueprintjs/core";
import moment from "moment";
import 'moment/locale/nl';
import gapi from "gapi-client";
import Entry from "./entry.js";
import SignIn from "./siginin";
import {
  GOOGLE_API_KEY,
  CALENDAR_ID
//  CLIENT_ID,
//  CLIENT_SECRET
} from "../config.js";

const numberofdays = 49;

export default class App extends Component {
  constructor(props) {
    super(props);
    moment.locale('nl');
    this.state = {
      time: moment().format("dd, D MMMM YYYY, HH:mm"),
      events: [],
      isLoading: true,
      isLoggedIn: false,
      userName: "",
      userPhoto: false,
      userToken: false
    };
  }

  handleLogin = (profile) => {
    this.setState({
      isLoggedIn: true,
      userName: profile.name,
      userPhoto: profile.url,
      userToken: profile.token
    })
  }

  componentDidMount = () => {
    this.getEvents();
  };

  getEvents() {
    var that = this;
    let today = moment().startOf('day');
    let lastday = moment(today).add(numberofdays, 'days');
    function start() {
      gapi.client
        .init({
          apiKey: GOOGLE_API_KEY
        })
        .then(function() {
            let path = `https://www.googleapis.com/calendar/v3/calendars/${CALENDAR_ID}/events?timeMin=${today.toISOString()}&timeMax=${lastday.toISOString()}`;
          console.log(path);
          return gapi.client.request({
            path: path
          });
        })
        .then(
          response => {
            //console.log(response.result);
            let events = response.result.items;
            let sortedEvents = events.sort(function(a, b) {
              return (
                moment(a.start.dateTime).format("YYYYMMDD") -
                moment(b.start.dateTime).format("YYYYMMDD")
              );
            });
            if (events.length > 0) {
              that.setState(
                {
                  events: sortedEvents,
                  isLoading: false,
                  isEmpty: false
                }
              );
            } else {
              that.setState({
                events: [],
                isEmpty: true,
                isLoading: false
              });
            }
          },
          function(reason) {
            console.log("What am I doing here?");
          }
        );
    }
    gapi.load("client", start);
  }

  render() {
    const { events } = this.state;

    let bookings = [];
    for (var i = 0; i < events.length; i++) {
      let key = moment(events[i].start.dateTime).format("YYYYMMDD");
      bookings[key] = events[i];
    }

    let days = [];
    let d = moment();
    for (let i = 0; i < numberofdays; i++) {
      if (d.format("e") > 3) {
        let d2=moment(d);
        let key = moment(d2).format("YYYYMMDD");
        let entry = bookings[key];
        if (typeof entry !== 'undefined') {
          days.push({day: d2, event: entry});
        } else {
          days.push({day: d2});
        }
      }
      d = d.add(1, 'days');
    }

    console.log("DAYS=" + days);

    let eventsList = (
      <table><tbody>
      {
        days.map((i) => {
            let key = moment(i.day).format("YYYYMMDD");
            return (
              <Entry entry={i} key={key} name={this.state.userName}/>
            );
          }
        )
      }
      </tbody></table>
    )

    let loadingState = (
      <div className="loading">
        <Spinner/>
        <p>Loading</p>
      </div>
    );

    let signedin = (
      <div className="auth-name">
        <p>Ingelogd als: {this.state.userName}</p>
      </div>
    );
//    <img className="auth-photo" src={this.state.userPhoto}/>

    let notsignedin = (
      <p className="auth-name">Niet ingelogd</p>
    );

    return (
      <div className="container">
        <header className="authentication">
          <SignIn className="auth-button" onLogin={this.handleLogin}/>
          {this.state.isLoggedIn && signedin}
          {!this.state.isLoggedIn && notsignedin}
          <h1>Royal Teazer Planner</h1>
          <hr/>
        </header>
        <div className="upcoming-meetings">
            {this.state.isLoading && loadingState}
            {(!this.state.isLoading) && eventsList}
        </div>
      </div>
    );
  }
}
