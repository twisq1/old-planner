import React, { Component } from "react";
import { Button, Classes, ButtonGroup, Tooltip, Icon, Spinner } from "@blueprintjs/core";
import moment from "moment";
import 'moment/locale/nl';
import gapi from "gapi-client";

import {
    GOOGLE_API_KEY,
    CALENDAR_ID,
    CLIENT_ID,
  //  CLIENT_SECRET
  } from "../config.js";
  
export default class Entry extends Component {

  constructor(props) {
    super(props);
    moment.locale('nl');
    if (this.props.entry.event == null) {
        this.state = {
            isLoading: false,
            isBooked: false,
            isAvailable: true,
            day: this.props.entry.day,
            start: null,
            yes: [],
            no: [],
            comments: '',
            eventId: null
        }
    } else {
        let parts = [];
        if (this.props.entry.event.hasOwnProperty('description')) {
            parts = this.props.entry.event.description.split(';');
        }
        let no = [];
        let yes = [];
        let comments = '';
        if (parts.length >= 1) {
            if (parts[0].length > 0) no = parts[0].split(',');
        }
        if (parts.length >= 2) {
            if (parts[1].length > 0) yes = parts[1].split(',');
        }
        if (parts.length >= 3) {
            comments = parts[2];
        }

        this.state = {
            isLoading: false,
            isBooked: (this.props.entry.event.summary === "Oefenen"),
            isAvailable: (no.length === 0),
            day: this.props.entry.day,
            start: this.props.entry.event.start.dateTime,
            no: no,
            yes: yes,
            comments: comments,
            eventId: this.props.entry.event.id
        }
    }
  }

  currentStateToEvent() {
      let start;
      let end;
      let summary;
      if (this.state.isBooked) {
        summary = 'Oefenen';
        start = moment(this.props.entry.day).hour(20).minute(30);
        end = moment(this.props.entry.day).hour(23).minute(59);
    } else if (this.state.no.length > 0) {
          summary = 'niet beschikbaar';
          start = moment(this.props.entry.day).startOf('day');
          end = moment(this.props.entry.day).endOf('day');
      } else {
          summary = 'beschikbaar';
          start = moment(this.props.entry.day).hour(1).minute(0);
          end = moment(this.props.entry.day).hour(1).minute(30);
      }

      let description = '';
      for (let i = 0; i < this.state.no.length; i++) {
          if (i > 0) description += ',';
          description += this.state.no[i];
      }
      description += ';';
      for (let i = 0; i < this.state.yes.length; i++) {
        if (i > 0) description += ',';
        description += this.state.yes[i];
      }
      description += ';';
      description += this.state.comments;

      let event = {
          'summary': summary,
          'description': description,
          'start': {
              'dateTime': start.toISOString(),
              'timeZone': 'Europe/Amsterdam'
          },
          'end': {
              'dateTime': end.toISOString(),
              'timeZone': 'Europe/Amsterdam'
          }
      }
      console.log("Event created: " + event);
      return event;
  }

  addCalendarEvent() {
    this.setState({
        isLoading: true
    })

    var event = this.currentStateToEvent();

      // Array of API discovery doc URLs for APIs used by the quickstart
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

      gapi.client.init({
        apiKey: GOOGLE_API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: 'https://www.googleapis.com/auth/calendar'
      })
      .then(() => {

        let path = `https://www.googleapis.com/calendar/v3/calendars/${CALENDAR_ID}/events`;
        console.log(path);
        return gapi.client.request({
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            path: path,
            body: JSON.stringify(event)
        });
      })
      .then((response => {
          console.log(response);
          if (response.status === 200) {
            this.setState({
                isLoading: false,
                eventId: response.result.id
            });
        } else {
            this.setState({
                isLoading: false
            });
            alert("Error: " + response.statusText);
        }
      }))
      .catch((exception) => {
        this.setState({
            isLoading: false
        });
    alert("Exception: " + exception);
    });
  }

  updateCalendarEvent() {
    this.setState({
        isLoading: true
    })

    var event = this.currentStateToEvent();

      // Array of API discovery doc URLs for APIs used by the quickstart
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

      gapi.client.init({
        apiKey: GOOGLE_API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: 'https://www.googleapis.com/auth/calendar'
      })
      .then(() => {
        let id = this.state.eventId;
        let path = `https://www.googleapis.com/calendar/v3/calendars/${CALENDAR_ID}/events/${id}`;
        console.log(path);
        return gapi.client.request({
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            path: path,
            body: JSON.stringify(event)
        });
      })
      .then((response => {
          console.log(response);
          if (response.status === 200) {
            this.setState({
                isLoading: false,
                eventId: response.result.id
            });
        } else {
            this.setState({
                isLoading: false
            });
            alert("Error: " + response.statusText);
        }
      }))
      .catch((exception) => {
        this.setState({
            isLoading: false
        });
    alert("Exception: " + exception);
    console.log(exception);
    });
  }

  deleteEvent() {
    this.setState({
        isLoading: true
    })

      // Array of API discovery doc URLs for APIs used by the quickstart
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

      gapi.client.init({
        apiKey: GOOGLE_API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: 'https://www.googleapis.com/auth/calendar'
      })
      .then(() => {
        let id = this.state.eventId;
        let path = `https://www.googleapis.com/calendar/v3/calendars/${CALENDAR_ID}/events/${id}`;
        console.log(path);
        return gapi.client.request({
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            path: path
        });
      })
      .then((response => {
          console.log(response);
          if ((response.status === 200) || (response.status === 204)) {
            this.setState({
                isLoading: false,
                isBooked: false,
                isAvailable: true,
                start: null,
                eventId: null
            })
        } else {
            this.setState({
                isLoading: false
            });
            alert("Error: " + response.statusText);
        }
      }))
      .catch((exception) => {
        this.setState({
            isLoading: false
        });
        alert("Exception: " + exception);
        console.log(exception);
      });

  }

  updateCalendar() {
      if (!this.state.eventId) {
          console.log("Creating calendar event.");
          console.log(this.state);
          this.addCalendarEvent();
      } else {
        console.log("Updating calendar.");
        console.log(this.state);
        this.updateCalendarEvent();
      }
  }

  makeBooking(e) {
      e.preventDefault();
      let newstate = {
          isBooked: true,
          start: moment(this.props.entry.day).hour(20).minute(30),
          end: moment(this.props.entry.day).hour(23).minute(59)
      };
      this.setState(newstate, () => {
        this.updateCalendar();
      });
  }

  cancelBooking(e) {
      e.preventDefault();
      this.setState({
          isBooked: false
      }, () => {this.updateCalendar()});
  }

  sayYes(e) {
      e.preventDefault();
      let yes = this.state.yes;
      let no = this.state.no;
        let change = false;
      if (this.state.yes.indexOf(this.props.name) < 0) {
        yes.push(this.props.name);
        change = true;
      }
      if (this.state.no.indexOf(this.props.name) > -1) {
          no.splice(this.state.no.indexOf(this.props.name), 1);
          change = true;
      }
      if (change) {
        this.setState({
            yes: yes,
            no: no,
            isAvailable: (no.length === 0)
        }, () => {this.updateCalendar()});
    }
  }

  sayNo(e) {
    e.preventDefault();
    let yes = this.state.yes;
    let no = this.state.no;
    let change = false;
    if (this.state.no.indexOf(this.props.name) < 0) {
      no.push(this.props.name);
      change = true;
    }
    if (this.state.yes.indexOf(this.props.name) > -1) {
        yes.splice(this.state.yes.indexOf(this.props.name), 1);
        change = true;
    }
    if (change) {
        this.setState({
            isAvailable: false,
            yes: yes,
            no: no
        }, () => {this.updateCalendar()});
    }
}

  render() {
      let cn = "";
      // eslint-disable-next-line
      if (moment(this.props.entry.day).format("e") == 6) cn = "sunday";
      
      let nostring = '';
      for (let i = 0; i < this.state.no.length; i++) {
          if (i > 0) nostring = nostring + ',';
          nostring = nostring + this.state.no[i];
      }
      let yesstring = '';
      for (let i = 0; i < this.state.yes.length; i++) {
          if (i > 0) yesstring += ',';
          yesstring += this.state.yes[i];
      }

      if (this.state.isLoading) {
        return (
            <tr className={cn}>
                <td className="tabcell" colSpan="5"><Spinner/></td>
            </tr>
        );
      } else if (this.state.isBooked) {
        return (
            <tr className={cn}>
                <td className="tabcell booked">{this.state.day.format("dddd")}</td>
                <td className="tabcell booked">{this.state.day.format("D MMMM")}</td>
                <td className="tabcell booked">{moment(this.state.start).format("HH:mm")}</td>
                <td className="tabcell booked">{this.state.comments}</td>
                <td className="tabcell">
                            <ButtonGroup>
                            <Tooltip content="Ik kan wel.">
                                <Button icon="small-tick" className={Classes.INTENT_SUCCESS} disabled/>
                            </Tooltip>
                            <Tooltip content="Ik kan niet.">
                                <Button icon="small-cross" className={Classes.INTENT_DANGER} disabled/>
                            </Tooltip>
                            </ButtonGroup>
                            &nbsp;
                            <Tooltip content="We gaan niet oefenen.">
                            <Button icon="hand" className={Classes.INTENT_WARNING} onClick={(e) => this.cancelBooking(e)}/>
                            </Tooltip>
                        </td>
            </tr>
        );
        } else {
            if (this.state.isAvailable) {
                return (
                    <tr className={cn}>
                        <td className="tabcell">{this.state.day.format("dddd")}</td>
                        <td className="tabcell">{this.state.day.format("D MMMM")}</td>
                        <td className="tabcell">&nbsp;</td>
                        <td className="tabcell"><span className="yes">{yesstring}</span>&nbsp;<span className="no">{nostring}</span></td>
                        <td className="tabcell">
                            <ButtonGroup>
                            <Tooltip content="Ik kan wel.">
                                <Button icon="small-tick" className={Classes.INTENT_SUCCESS} onClick={(e) => this.sayYes(e)}/>
                            </Tooltip>
                            <Tooltip content="Ik kan niet.">
                                <Button icon="small-cross" className={Classes.INTENT_DANGER} onClick={(e) => this.sayNo(e)}/>
                            </Tooltip>
                            </ButtonGroup>
                            &nbsp;
                            <Tooltip content="We gaan oefenen.">
                            <Button icon="take-action" className={Classes.INTENT_PRIMARY} onClick={(e) => this.makeBooking(e)}/>
                            </Tooltip>
                        </td>
                    </tr>
                );
            } else {
                return (
                    <tr className={cn}>
                        <td className="tabcell unavailable">{this.state.day.format("dddd")}</td>
                        <td className="tabcell unavailable">{this.state.day.format("D MMMM")}</td>
                        <td className="tabcell unavailable">
                          <Tooltip content="Deze datum is niet beschikbaar.">
                            <Icon icon="ban-circle"/>
                          </Tooltip>
                        </td>
                        <td className="tabcell"><span className="yes">{yesstring}</span>&nbsp;<span className="no">{nostring}</span></td>
                        <td className="tabcell">
                            <ButtonGroup>
                            <Tooltip content="Ik kan wel.">
                                <Button icon="small-tick" className={Classes.INTENT_SUCCESS} onClick={(e) => this.sayYes(e)}/>
                            </Tooltip>
                            <Tooltip content="Ik kan niet.">
                                <Button icon="small-cross" className={Classes.INTENT_DANGER} onClick={(e) => this.sayNo(e)}/>
                            </Tooltip>
                            </ButtonGroup>
                            &nbsp;
                            <Tooltip content="We gaan oefenen.">
                            <Button icon="take-action" className={Classes.INTENT_PRIMARY} onClick={(e) => this.makeBooking(e)}/>
                            </Tooltip>
                        </td>
                    </tr>
                );
            }
        }
    }
}
