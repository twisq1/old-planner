import React, { Component } from "react";
import gapi from "gapi-client";

export default class SiginIn extends Component {

	render() {
//        <div id="g-signin2" data-onsuccess={this.onSignIn.bind(this)} />
		return (
            <div className="auth-buttons">
			    <div id="g-signin2" />
            </div>
        );
	}

	componentDidMount() {
		gapi.signin2.render('g-signin2', {
			'scope': 'https://www.googleapis.com/auth/calendar',
			'width': 320,
			'height': 50,
			'longtitle': true,
			'theme': 'dark',
			'onsuccess': this.onSignIn.bind(this)
		});
	}

	onSignIn(googleUser) {
        let name = googleUser.getBasicProfile().getGivenName();
        let token = googleUser.getAuthResponse().id_token;
        let url = googleUser.getBasicProfile().getImageUrl();
        let profile = {
            name: name,
            url: url,
            token: token
        }
        console.log(name);
        this.props.onLogin(profile);
		// sessionStorage.setItem('authToken', profile.getId());
		// sessionStorage.setItem('name', profile.getName());
		// sessionStorage.setItem('imageUrl', profile.getImageUrl());
		// sessionStorage.setItem('email', profile.getEmail());

		// let account = this.props.cursor.refine('account');
		// account.refine('authToken').set(sessionStorage.getItem('authToken'));
		// account.refine('name').set(sessionStorage.getItem('name'));
		// account.refine('imageUrl').set(sessionStorage.getItem('imageUrl'));
		// account.refine('email').set(sessionStorage.getItem('email'));
	}
}
